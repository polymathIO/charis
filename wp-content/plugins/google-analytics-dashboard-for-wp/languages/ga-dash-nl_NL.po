msgid ""
msgstr ""
"Project-Id-Version: Google Analytics Dashboard for WP\n"
"POT-Creation-Date: 2015-05-20 21:46+0200\n"
"PO-Revision-Date: 2015-05-20 21:46+0200\n"
"Last-Translator: Alin Marcu <admin@deconf.com>\n"
"Language-Team: Willem-Jan Meerkerk <info@bs-webdevelopment.nl>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SearchPath-0: ../.\n"

#: .././admin/item-reports.php:67
msgid "Analytics"
msgstr ""

#: .././admin/settings.php:94 .././admin/settings.php:197
#: .././admin/settings.php:328 .././admin/settings.php:800
#: .././admin/settings.php:1069
msgid "Settings saved."
msgstr ""

#: .././admin/settings.php:96 .././admin/settings.php:199
#: .././admin/settings.php:330 .././admin/settings.php:777
#: .././admin/settings.php:787 .././admin/settings.php:796
#: .././admin/settings.php:802 .././admin/settings.php:814
#: .././admin/settings.php:1030 .././admin/settings.php:1055
#: .././admin/settings.php:1065 .././admin/settings.php:1071
#: .././admin/settings.php:1083
msgid "Cheating Huh?"
msgstr ""

#: .././admin/settings.php:100 .././admin/settings.php:203
#: .././admin/settings.php:334 .././admin/settings.php:644
#: .././admin/settings.php:828 .././admin/settings.php:1096
#, php-format
msgid "Something went wrong, check %1$s or %2$s."
msgstr ""

#: .././admin/settings.php:100 .././admin/settings.php:203
#: .././admin/settings.php:334 .././admin/settings.php:644
#: .././admin/settings.php:828 .././admin/settings.php:1096
#: .././admin/setup.php:96 .././admin/setup.php:119
msgid "Errors & Debug"
msgstr ""

#: .././admin/settings.php:100 .././admin/settings.php:203
#: .././admin/settings.php:334 .././admin/settings.php:644
#: .././admin/settings.php:828 .././admin/settings.php:1096
msgid "authorize the plugin"
msgstr ""

#: .././admin/settings.php:105
msgid "Google Analytics Frontend Settings"
msgstr ""

#: .././admin/settings.php:115 .././admin/settings.php:218
msgid "Permissions"
msgstr ""

#: .././admin/settings.php:118 .././admin/settings.php:221
msgid "Show stats to:"
msgstr ""

#: .././admin/settings.php:157
msgid "show page sessions and users in frontend (after each article)"
msgstr ""

#: .././admin/settings.php:168
msgid "show page searches (after each article)"
msgstr ""

#: .././admin/settings.php:175 .././admin/settings.php:308
#: .././admin/settings.php:602 .././admin/settings.php:945
#: .././admin/settings.php:1233
msgid "Save Changes"
msgstr ""

#: .././admin/settings.php:208
msgid "Google Analytics Backend Settings"
msgstr ""

#: .././admin/settings.php:262
msgid "enable Switch View functionality"
msgstr ""

#: .././admin/settings.php:273
msgid "enable reports on Posts List and Pages List"
msgstr ""

#: .././admin/settings.php:284
msgid "enable the main Dashboard Widget"
msgstr ""

#: .././admin/settings.php:288
msgid "Real-Time Settings"
msgstr ""

#: .././admin/settings.php:291
msgid "Maximum number of pages to display on real-time tab:"
msgstr ""

#: .././admin/settings.php:296
msgid "Location Settings"
msgstr ""

#: .././admin/settings.php:300
msgid "Target Geo Map to country:"
msgstr ""

#: .././admin/settings.php:337
msgid ""
"The tracking component is disabled. You should set <strong>Tracking Options</"
"strong> to <strong>Enabled</strong>"
msgstr ""

#: .././admin/settings.php:342
msgid "Google Analytics Tracking Code"
msgstr ""

#: .././admin/settings.php:351
msgid "Basic Settings"
msgstr ""

#: .././admin/settings.php:352 .././admin/settings.php:418
msgid "Events Tracking"
msgstr ""

#: .././admin/settings.php:353 .././admin/settings.php:466
msgid "Custom Definitions"
msgstr ""

#: .././admin/settings.php:354 .././admin/settings.php:564
#: .././admin/settings.php:1216
msgid "Exclude Tracking"
msgstr ""

#: .././admin/settings.php:355
msgid "Advanced Settings"
msgstr ""

#: .././admin/settings.php:363
msgid "Tracking Settings"
msgstr ""

#: .././admin/settings.php:366
msgid "Tracking Options:"
msgstr ""

#: .././admin/settings.php:368
msgid "Disabled"
msgstr ""

#: .././admin/settings.php:369
msgid "Enabled"
msgstr ""

#: .././admin/settings.php:377 .././admin/settings.php:892
#: .././admin/settings.php:913 .././admin/settings.php:1189
#: .././admin/widgets.php:71
msgid "View Name:"
msgstr ""

#: .././admin/settings.php:377 .././admin/settings.php:913
msgid "Tracking ID:"
msgstr ""

#: .././admin/settings.php:377 .././admin/settings.php:913
msgid "Default URL:"
msgstr ""

#: .././admin/settings.php:377 .././admin/settings.php:913
msgid "Time Zone:"
msgstr ""

#: .././admin/settings.php:382
msgid "Basic Tracking"
msgstr ""

#: .././admin/settings.php:385
msgid "Tracking Type:"
msgstr ""

#: .././admin/settings.php:387
msgid "Classic Analytics"
msgstr ""

#: .././admin/settings.php:388
msgid "Universal Analytics"
msgstr ""

#: .././admin/settings.php:399
msgid "anonymize IPs while tracking"
msgstr ""

#: .././admin/settings.php:410
msgid "enable remarketing, demographics and interests reports"
msgstr ""

#: .././admin/settings.php:428
msgid "track downloads, mailto and outbound links"
msgstr ""

#: .././admin/settings.php:432
msgid "Downloads Regex:"
msgstr ""

#: .././admin/settings.php:443
msgid "track affiliate links matching this regex"
msgstr ""

#: .././admin/settings.php:447
msgid "Affiliates Regex:"
msgstr ""

#: .././admin/settings.php:458
msgid "track fragment identifiers, hashmarks (#) in URI links"
msgstr ""

#: .././admin/settings.php:469
msgid "Authors:"
msgstr ""

#: .././admin/settings.php:477
msgid "Publication Year:"
msgstr ""

#: .././admin/settings.php:485
msgid "Categories:"
msgstr ""

#: .././admin/settings.php:493
msgid "User Type:"
msgstr ""

#: .././admin/settings.php:505
msgid "Advanced Tracking"
msgstr ""

#: .././admin/settings.php:508
msgid "Page Speed SR:"
msgstr ""

#: .././admin/settings.php:519
msgid "exclude events from bounce-rate calculation"
msgstr ""

#: .././admin/settings.php:530
msgid "enable enhanced link attribution"
msgstr ""

#: .././admin/settings.php:541
msgid "enable AdSense account linking"
msgstr ""

#: .././admin/settings.php:552
msgid "enable cross domain tracking"
msgstr ""

#: .././admin/settings.php:556
msgid "Cross Domains:"
msgstr ""

#: .././admin/settings.php:567
msgid "Exclude tracking for:"
msgstr ""

#: .././admin/settings.php:648
msgid "Google Analytics Errors & Debugging"
msgstr ""

#: .././admin/settings.php:658
msgid "Errors & Details"
msgstr ""

#: .././admin/settings.php:659
msgid "Plugin Settings"
msgstr ""

#: .././admin/settings.php:667
msgid "Last Error detected"
msgstr ""

#: .././admin/settings.php:673 .././admin/settings.php:686
msgid "None"
msgstr ""

#: .././admin/settings.php:680
msgid "Error Details"
msgstr ""

#: .././admin/settings.php:700
msgid "Plugin Configuration"
msgstr ""

#: .././admin/settings.php:722 .././admin/settings.php:980
msgid ""
"Loading the required libraries. If this results in a blank screen or a fatal "
"error, try this solution:"
msgstr ""

#: .././admin/settings.php:722
msgid "Library conflicts between WordPress plugins"
msgstr ""

#: .././admin/settings.php:737 .././admin/settings.php:997
msgid "Plugin authorization succeeded."
msgstr ""

#: .././admin/settings.php:752 .././admin/settings.php:1021
msgid ""
"The access code is <strong>NOT</strong> your <strong>Tracking ID</strong> "
"(UA-XXXXX-X). Try again, and use the red link to get your access code"
msgstr ""

#: .././admin/settings.php:775 .././admin/settings.php:1053
msgid "Cleared Cache."
msgstr ""

#: .././admin/settings.php:784 .././admin/settings.php:1062
msgid "Token Reseted and Revoked."
msgstr ""

#: .././admin/settings.php:794
msgid "All errors reseted."
msgstr ""

#: .././admin/settings.php:807 .././admin/settings.php:1076
msgid "All other domains/properties were removed."
msgstr ""

#: .././admin/settings.php:819 .././admin/settings.php:1088
msgid "Google Analytics Settings"
msgstr ""

#: .././admin/settings.php:833 .././admin/settings.php:1101
msgid "Use the red link (see below) to generate and get your access code!"
msgstr ""

#: .././admin/settings.php:844 .././admin/settings.php:1129
msgid "Plugin Authorization"
msgstr ""

#: .././admin/settings.php:849 .././admin/settings.php:1133
#, php-format
msgid ""
"You should watch the %1$s and read this %2$s before proceeding to "
"authorization. This plugin requires a properly configured Google Analytics "
"account!"
msgstr ""

#: .././admin/settings.php:849 .././admin/settings.php:1133
msgid "video"
msgstr ""

#: .././admin/settings.php:849 .././admin/settings.php:1133
msgid "tutorial"
msgstr ""

#: .././admin/settings.php:854 .././admin/settings.php:1140
msgid "use your own API Project credentials"
msgstr ""

#: .././admin/settings.php:858 .././admin/settings.php:1147
msgid "API Key:"
msgstr ""

#: .././admin/settings.php:862 .././admin/settings.php:1151
msgid "Client ID:"
msgstr ""

#: .././admin/settings.php:866 .././admin/settings.php:1155
msgid "Client Secret:"
msgstr ""

#: .././admin/settings.php:876 .././admin/settings.php:1165
msgid "Clear Authorization"
msgstr ""

#: .././admin/settings.php:876 .././admin/settings.php:951
#: .././admin/settings.php:1165 .././admin/settings.php:1240
msgid "Clear Cache"
msgstr ""

#: .././admin/settings.php:876
msgid "Reset Errors"
msgstr ""

#: .././admin/settings.php:882 .././admin/setup.php:80
#: .././admin/setup.php:115
msgid "General Settings"
msgstr ""

#: .././admin/settings.php:885
msgid "Select View:"
msgstr ""

#: .././admin/settings.php:896 .././admin/settings.php:1193
msgid "Property not found"
msgstr ""

#: .././admin/settings.php:901
msgid "Lock Selection"
msgstr ""

#: .././admin/settings.php:919
msgid "Theme Color:"
msgstr ""

#: .././admin/settings.php:927 .././admin/settings.php:1202
msgid "Automatic Updates"
msgstr ""

#: .././admin/settings.php:937 .././admin/settings.php:1212
msgid ""
"automatic updates for minor versions (security and maintenance releases only)"
msgstr ""

#: .././admin/settings.php:951 .././admin/settings.php:1240
#: .././admin/widgets.php:42
msgid "Authorize Plugin"
msgstr ""

#: .././admin/settings.php:1027
msgid "Properties refreshed."
msgstr ""

#: .././admin/settings.php:1111
msgid "Network Setup"
msgstr ""

#: .././admin/settings.php:1121
msgid "use a single Google Analytics account for the entire network"
msgstr ""

#: .././admin/settings.php:1165
msgid "Refresh Properties"
msgstr ""

#: .././admin/settings.php:1171
msgid "Properties/Views Settings"
msgstr ""

#: .././admin/settings.php:1226
msgid "exclude Super Admin tracking for the entire network"
msgstr ""

#: .././admin/settings.php:1271
msgid "Setup Tutorial & Demo"
msgstr ""

#: .././admin/settings.php:1279
msgid "Support & Reviews"
msgstr ""

#: .././admin/settings.php:1286
#, php-format
msgid "Plugin documentation and support on %s"
msgstr ""

#: .././admin/settings.php:1293
#, php-format
msgid "Your feedback and review are both important, %s!"
msgstr ""

#: .././admin/settings.php:1293
msgid "rate this plugin"
msgstr ""

#: .././admin/settings.php:1299
msgid "Further Reading"
msgstr ""

#: .././admin/settings.php:1306
#, php-format
msgid "%s by moving your website to HTTPS/SSL."
msgstr ""

#: .././admin/settings.php:1306
msgid "Improve search rankings"
msgstr ""

#: .././admin/settings.php:1313
#, php-format
msgid "Other %s written by the same author"
msgstr ""

#: .././admin/settings.php:1313
msgid "WordPress Plugins"
msgstr ""

#: .././admin/settings.php:1319
msgid "Other Services"
msgstr ""

#: .././admin/settings.php:1326
#, php-format
msgid "Speed up your website and plug into a whole %s"
msgstr ""

#: .././admin/settings.php:1326
msgid "new level of site speed"
msgstr ""

#: .././admin/settings.php:1333
#, php-format
msgid "%s service with users tracking at IP level."
msgstr ""

#: .././admin/settings.php:1333
msgid "Web Analytics"
msgstr ""

#: .././admin/setup.php:76 .././admin/setup.php:111
msgid "Google Analytics"
msgstr ""

#: .././admin/setup.php:84
msgid "Backend Settings"
msgstr ""

#: .././admin/setup.php:88
msgid "Frontend Settings"
msgstr ""

#: .././admin/setup.php:92
msgid "Tracking Code"
msgstr ""

#: .././admin/setup.php:195 .././admin/widgets.php:126
msgid "Today"
msgstr "Vandaag"

#: .././admin/setup.php:196 .././admin/widgets.php:127
msgid "Yesterday"
msgstr "Gisteren"

#: .././admin/setup.php:197 .././admin/widgets.php:128
#: .././front/widgets.php:74 .././front/widgets.php:182
msgid "Last 7 Days"
msgstr "Afgelopen 7 dagen"

#: .././admin/setup.php:198 .././admin/widgets.php:130
#: .././front/widgets.php:80 .././front/widgets.php:184
msgid "Last 30 Days"
msgstr "Afgelopen 30 dagen"

#: .././admin/setup.php:199 .././admin/widgets.php:131
msgid "Last 90 Days"
msgstr "Afgelopen 90 dagen"

#: .././admin/setup.php:202 .././admin/setup.php:218
msgid "Unique Views"
msgstr ""

#: .././admin/setup.php:203 .././admin/setup.php:219
#: .././admin/widgets.php:136 .././admin/widgets.php:875
#: .././tools/gapi.php:391
msgid "Users"
msgstr "Bezoekers"

#: .././admin/setup.php:204 .././admin/widgets.php:137
msgid "Organic"
msgstr "Via zoekmachines"

#: .././admin/setup.php:205 .././admin/setup.php:220
#: .././admin/widgets.php:138 .././admin/widgets.php:879
#: .././tools/gapi.php:394
msgid "Page Views"
msgstr "Paginaweergaven"

#: .././admin/setup.php:206 .././admin/setup.php:221
#: .././admin/widgets.php:139 .././admin/widgets.php:883
#: .././tools/gapi.php:397
msgid "Bounce Rate"
msgstr "Bouncepercentage"

#: .././admin/setup.php:207 .././admin/widgets.php:140
msgid "Location"
msgstr "Locaties"

#: .././admin/setup.php:208 .././admin/widgets.php:142 .././tools/gapi.php:566
msgid "Referrers"
msgstr "Beste verwijzingen"

#: .././admin/setup.php:209 .././admin/widgets.php:143 .././tools/gapi.php:612
#: .././tools/gapi.php:918
msgid "Searches"
msgstr "Beste zoekopdrachten"

#: .././admin/setup.php:210 .././admin/widgets.php:144
msgid "Traffic Details"
msgstr "Verkeer"

#: .././admin/setup.php:213 .././admin/widgets.php:510
#: .././admin/widgets.php:607 .././admin/widgets.php:805
#: .././admin/widgets.php:917 .././front/item-reports.php:97
msgid "A JavaScript Error is blocking plugin resources!"
msgstr ""

#: .././admin/setup.php:214 .././admin/widgets.php:713
msgid "Traffic Mediums"
msgstr "Verkeersbronnen"

#: .././admin/setup.php:215 .././admin/widgets.php:729
msgid "Visitor Type"
msgstr "Nieuw vs. terugkerend"

#: .././admin/setup.php:216 .././admin/widgets.php:745
msgid "Social Networks"
msgstr "Sociale netwerken"

#: .././admin/setup.php:217 .././admin/widgets.php:761
msgid "Search Engines"
msgstr "Zoekmachines"

#: .././admin/setup.php:222 .././admin/widgets.php:887
msgid "Organic Search"
msgstr "Zoekopdrachten"

#: .././admin/setup.php:223 .././admin/widgets.php:891
msgid "Pages/Session"
msgstr "Pagina's/bezoek"

#: .././admin/setup.php:224 .././admin/widgets.php:523
#: .././admin/widgets.php:541 .././admin/widgets.php:620
#: .././admin/widgets.php:638 .././admin/widgets.php:657
#: .././admin/widgets.php:676 .././admin/widgets.php:696
#: .././admin/widgets.php:819 .././admin/widgets.php:930
#: .././admin/widgets.php:949 .././front/item-reports.php:109
#: .././front/item-reports.php:128
msgid "Invalid response, more details in JavaScript Console (F12)."
msgstr ""

#: .././admin/setup.php:225
msgid "Not enough data collected"
msgstr ""

#: .././admin/setup.php:226 .././admin/widgets.php:528
#: .././admin/widgets.php:546 .././admin/widgets.php:625
#: .././admin/widgets.php:643 .././admin/widgets.php:662
#: .././admin/widgets.php:681 .././admin/widgets.php:701
#: .././admin/widgets.php:824 .././admin/widgets.php:827
#: .././admin/widgets.php:935 .././admin/widgets.php:954
#: .././front/item-reports.php:114 .././front/item-reports.php:133
#: .././front/widgets.php:110
msgid "This report is unavailable"
msgstr ""

#: .././admin/setup.php:227
msgid "report generated by"
msgstr ""

#: .././admin/setup.php:267
msgid "Settings"
msgstr ""

#: .././admin/widgets.php:33 .././front/widgets.php:22
msgid "Google Analytics Dashboard"
msgstr "Bezoekersstatistieken"

#: .././admin/widgets.php:42
msgid "This plugin needs an authorization:"
msgstr ""

#: .././admin/widgets.php:76
msgid "Something went wrong while retrieving profiles list."
msgstr ""

#: .././admin/widgets.php:76
msgid "More details"
msgstr ""

#: .././admin/widgets.php:89 .././admin/widgets.php:100
msgid "An admin should asign a default Google Analytics Profile."
msgstr ""

#: .././admin/widgets.php:89 .././admin/widgets.php:100
msgid "Select Domain"
msgstr ""

#: .././admin/widgets.php:105
msgid ""
"Something went wrong while retrieving property data. You need to create and "
"properly configure a Google Analytics account:"
msgstr ""

#: .././admin/widgets.php:105
msgid "Find out more!"
msgstr ""

#: .././admin/widgets.php:125
msgid "Real-Time"
msgstr "Realtime"

#: .././admin/widgets.php:129 .././front/widgets.php:77
#: .././front/widgets.php:183
msgid "Last 14 Days"
msgstr "Afgelopen 14 dagen"

#: .././admin/widgets.php:135 .././admin/widgets.php:871
#: .././front/widgets.php:46 .././tools/gapi.php:406 .././tools/gapi.php:567
#: .././tools/gapi.php:613 .././tools/gapi.php:676 .././tools/gapi.php:786
#: .././tools/gapi.php:827 .././tools/gapi.php:919
msgid "Sessions"
msgstr "Bezoeken"

#: .././admin/widgets.php:141 .././tools/gapi.php:521
msgid "Pages"
msgstr "Meest bezochte pagina's"

#: .././admin/widgets.php:232 .././admin/widgets.php:472
msgid "REFERRAL"
msgstr "Verwijzend"

#: .././admin/widgets.php:236 .././admin/widgets.php:473
msgid "ORGANIC"
msgstr "Zoekmachines"

#: .././admin/widgets.php:240 .././admin/widgets.php:360
#: .././admin/widgets.php:474
msgid "SOCIAL"
msgstr "Sociale media"

#: .././admin/widgets.php:244 .././admin/widgets.php:363
#: .././admin/widgets.php:475
msgid "CAMPAIGN"
msgstr "Campagne"

#: .././admin/widgets.php:248 .././admin/widgets.php:366
#: .././admin/widgets.php:478
msgid "DIRECT"
msgstr "Direct"

#: .././admin/widgets.php:252 .././admin/widgets.php:479
msgid "NEW"
msgstr "Nieuw"

#: .././admin/widgets.php:354
msgid "REFERRALS"
msgstr "Verwijzingen"

#: .././admin/widgets.php:357
msgid "KEYWORDS"
msgstr "Zoekwoorden"

#: .././front/item-reports.php:143
msgid "Views vs UniqueViews"
msgstr ""

#: .././front/item-reports.php:193
msgid "Google Analytics Reports"
msgstr ""

#: .././front/widgets.php:23
msgid "Will display your google analytics stats in a widget"
msgstr ""

#: .././front/widgets.php:46 .././tools/gapi.php:827
msgid "trend"
msgstr ""

#: .././front/widgets.php:133
msgid "Period:"
msgstr ""

#: .././front/widgets.php:133
msgid "Sessions:"
msgstr ""

#: .././front/widgets.php:137
msgid "generated by"
msgstr ""

#: .././front/widgets.php:147
msgid "Google Analytics Stats"
msgstr ""

#: .././front/widgets.php:154
msgid "Title:"
msgstr ""

#: .././front/widgets.php:161
msgid "Display:"
msgstr ""

#: .././front/widgets.php:165
msgid "Chart & Totals"
msgstr ""

#: .././front/widgets.php:166
msgid "Chart"
msgstr ""

#: .././front/widgets.php:167
msgid "Totals"
msgstr ""

#: .././front/widgets.php:171
msgid "Anonymize stats:"
msgstr ""

#: .././front/widgets.php:178
msgid "Stats for:"
msgstr ""

#: .././front/widgets.php:188
msgid "Give credits:"
msgstr ""

#: .././gadwp.php:46 .././gadwp.php:55 .././gadwp.php:63
msgid "This is not allowed, read the documentation!"
msgstr ""

#: .././tools/gapi.php:134
msgid "Use this link to get your access code:"
msgstr ""

#: .././tools/gapi.php:134
msgid "Get Access Code"
msgstr ""

#: .././tools/gapi.php:138 .././tools/gapi.php:139
msgid "Use the red link to get your access code!"
msgstr ""

#: .././tools/gapi.php:138
msgid "Access Code:"
msgstr ""

#: .././tools/gapi.php:145
msgid "Save Access Code"
msgstr ""

#: .././tools/gapi.php:400
msgid "Organic Searches"
msgstr "Zoekopdrachten"

#: .././tools/gapi.php:403
msgid "Unique Page Views"
msgstr ""

#: .././tools/gapi.php:411
msgid "Hour"
msgstr "Uur"

#: .././tools/gapi.php:414 .././tools/gapi.php:826 .././tools/gapi.php:876
msgid "Date"
msgstr "Datum"

#: .././tools/gapi.php:522 .././tools/gapi.php:877
msgid "Views"
msgstr "Weergaven"

#: .././tools/gapi.php:640
msgid "Countries"
msgstr "Landen"

#: .././tools/gapi.php:650
msgid "Cities from"
msgstr ""

#: .././tools/gapi.php:722
msgid "Channels"
msgstr ""

#: .././tools/gapi.php:785
msgid "Type"
msgstr ""

#: .././tools/gapi.php:878
msgid "UniqueViews"
msgstr ""

#~ msgid "Top Searches"
#~ msgstr "Beste zoekopdrachten"

#~ msgid ""
#~ " (watch this <a href='http://deconf.com/projects/google-analytics-"
#~ "dashboard-for-wordpress/' target='_blank'>Step by step video tutorial</a>)"
#~ msgstr ""
#~ " (watch this <a href='http://deconf.com/projects/google-analytics-"
#~ "dashboard-for-wordpress/' target='_blank'>Step by step video tutorial</a>)"

#~ msgid ""
#~ "No stats available. Please check the Debugging Data section for possible "
#~ "errors"
#~ msgstr "Geen statistieken beschikbaar."

#~ msgid "Source"
#~ msgstr "Bron"

#~ msgid "New vs. Returning"
#~ msgstr "Nieuw vs. terugkerend"

#~ msgid "RETURN"
#~ msgstr "Terugkerend"

#~ msgid "Traffic Overview"
#~ msgstr "Verkeer"

#~ msgid "Visits"
#~ msgstr "Bezoeken"

#~ msgid "Visits:"
#~ msgstr "Bezoeken:"

#~ msgid "Visitors:"
#~ msgstr "Bezoekers:"

#~ msgid "Page Views:"
#~ msgstr "Paginaweergaven:"

#~ msgid "Bounce Rate:"
#~ msgstr "Bouncepercentage:"

#~ msgid "Pages per Visit:"
#~ msgstr "Paginaweergaven per bezoek:"
