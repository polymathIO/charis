<?php
/*
Template Name: Search
*/
get_header(); ?>

<section class="panel first">
	<div class="container">
		<div class="grid feat-posts">
			<div class="col-100 bootstrap" >
				<h2 class="thin">Sources: Advanced Search</h2>
			</div>
			<div class="col-100 bootstrap" >
				<p>This page is an advanced discovery tool for searching historical resources related to the Churches of Christ.  Currently, this page searches two online databases:</p>
				<p>1) The Digital Commons @ ACU</p>
				<p>2) The Abilene Christian University Library Collection, Portal of Texas History.</p>
				<p>Enter search terms and select the appropriate field from the “keyword” button. Adding search terms and date ranges can refine the search.</p>
			</div>
			<div class="col-50 bootstrap" id="search-terms">
				<h3 class="thin title">Search Terms</h3>
				<form action="/results" method="GET">
					<div id="termBoxes">
						<div class="search-term">
							 <div class="input-group">
							 	<a class="input-group-addon bg-danger removeST">
							 		<span class="glyphicon glyphicon-remove"></span> Remove
							 	</a>

						      <input name="Keyword[]" type="text" class="form-control">
						      <div class="input-group-btn">
						        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"><name>Keyword</name> <span class="caret"></span></button>
						        <ul class="dropdown-menu dropdown-menu-right" role="menu">
						          	<li><a class="select-query" href="#">Keyword</a></li>
						          	<li><a class="select-query" href="#">Title</a></li>
								    <li><a class="select-query" href="#">Creator</a></li>
								    <li><a class="select-query" href="#">Subject</a></li>
								    <li><a class="select-query" href="#">Description</a></li>
								    <li><a class="select-query" href="#">Publisher</a></li>
								    <li><a class="select-query" href="#">Contributor</a></li>
								    <li><a class="select-query" href="#">Type</a></li>
								    <li><a class="select-query" href="#">Format</a></li>
								    <li><a class="select-query" href="#">Identifier</a></li>
								    <li><a class="select-query" href="#">Source</a></li>
								    <li><a class="select-query" href="#">Language</a></li>
								    <li><a class="select-query" href="#">Relation</a></li>
								    <li><a class="select-query" href="#">Coverage</a></li>
								    <li><a class="select-query" href="#">Rights</a></li>
						          <!--li class="divider"></li>
						          <li disabled><a class="select-query" href="#">Everything</a></li> -->
						        </ul>
						      </div><!-- /btn-group -->
						    </div><!-- /input-group -->
						</div>
					</div>
				<span class="dropdown">
				  <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
				  	<span class="glyphicon glyphicon-plus-sign"></span> Add a Search Term
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="selectST" data-cta="and">AND</a></li>
				    <li role="presentation" ><a role="menuitem" tabindex="-1" href="#" class="selectST" data-cta="or">OR</a></li>
				    <li role="presentation" ><a role="menuitem" tabindex="-1" href="#" class="selectST" data-cta="not">NOT</a></li>
				</span>
			</div>
			<div class="col-50 bootstrap toggleDateRange">
				<h3 class="thin title">Resource Date</h3>
				<div class="btn-group btn-group-justified" data-toggle="buttons">
				  <label class="btn btn-info active">
				    <input type="radio" name="options" id="range" checked> Range
				  </label>
				  <label class="btn btn-info">
				    <input type="radio" name="options" id="before"> Before
				  </label>
				  <label class="btn btn-info">
				    <input type="radio" name="options" id="after"> After
				  </label>
				</div>
			    <div class="date">
				    <div class="input-group">
					  <span class="input-group-addon">From</span>
					  <input name="after" type="text" class="form-control" placeholder="e.g. 1901-1-1" id="fromDate">
					  <div class="input-group-btn">
					  	<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-calendar"></span></button>
					  </div>
					</div>
			    </div>
			    <div class="date">
				    <div class="input-group">
					  <span class="input-group-addon">To</span>
					  <input name="before" type="text" class="form-control" placeholder="e.g. 1941-12-31" id="toDate">
					  <div class="input-group-btn">
					  	<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-calendar"></span></button>
					  </div>
					</div>
			    </div>

		</div>
		<div class="grid feat-posts">
			<!--<div class="col-50 bootstrap">
				<h3 class="thin title">Sources</h3>
				<div class="panel panel-default">
				  <div class="panel-body">
				  	<div class="checkbox">
					    <label>
					      <input disabled type="checkbox"> History
					    </label>
					  </div>
				  	<div class="checkbox">
					    <label>
					      <input disabled type="checkbox"> University
					    </label>
					  </div>
				  	<div class="checkbox">
					    <label>
					      <input disabled type="checkbox"> Church
					    </label>
					  </div>
				  	<div class="checkbox">
					    <label>
					      <input disabled type="checkbox"> Conversations
					    </label>
					  </div>
				  </div>
				</div>
								
			</div>-->
			
			<div class="col-33 col--push-33 bootstrap">
				<button type="submit" class="btn btn-lg btn-info btn-block">Search</button>
			</div>
			<div class="col-100 bootstrap">
				<h3 class="thin title">Document Type</h3>
				<div class="panel panel-default">
				  <div class="panel-body">
				  	<div class="col-50 bootstrap">
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value="Newspaper" type="checkbox">Newspaper
						    </label>
						  </div>
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value="Book" type="checkbox">Book
						    </label>
						  </div>
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value="Yearbook" type="checkbox">Yearbook
						    </label>
						  </div>
					</div>
				  	<div class="col-50 bootstrap">
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value="Artwork" type="checkbox">Artwork
						    </label>
						  </div>
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value="Journal/Magazine/Newsletter" type="checkbox">Journal/Magazine/Newsletter
						    </label>
						  </div>
					  	<div class="checkbox">
						    <label>
						      <input name="type[]" value=" Newspaper" type="checkbox"> Newspaper
						    </label>
						  </div>
					</div>
				  </div>
				</div>
			</div>
			</form>
		</div>
	</div>
</section>

-	
<section class="panel white">
	<div class="container">
		<div class="grid">
			<h2 class="col-100">About CHARIS</h2>
			<div class="col-66">
				<p class="rightpad">
					CHARIS hosts conversations of and about Churches of Christ.  The website is intended to support education for Christian life and community through contemporary discussions and historical sources that variously witness to the gifts (“charis”) of God among Churches of Christ, especially their plea for visible unity among Christians through ongoing renewal and restoration of Scriptural beliefs and practices among God’s people.
				</p>
				<p class="rightpad">
					The CHARIS website is supported by Abilene Christian University (Abilene, TX, USA), the Center for Heritage and Renewal in Spirituality (CHARIS) at ACU. The purpose of CHARIS at ACU is to seek God’s blessings for a healthy relationship between the Christian college/university – its faculty, staff, and students – and the church heritage that gives identity and meaning to such a school.  This underlying concern for Christian colleges/universities, and their relationship to the churches, is reflected in the form and content of the CHARIS website. 
				</p>
				<h3 class="title thin">Contact Us</h3>
 
				<?php echo do_shortcode( '[contact-form-7 id="9" title="Contact form"]' ); ?>
				
				
			</div>
			<div class="col-33">
				<div class="ad">
							<?php
							  $args=array(
							    'post_type' => 'ad',
							    'post_status' => 'publish',
							    'posts_per_page' => 1
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post();
			
							    
							    ?>
									<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
			
							}
							
							  
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
											    										
						</div>
			
			</div>
		</div>
	</div>
</section>
-
-
-    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
-    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
-    <script type="text/javascript" src="//use.typekit.net/bst0pmp.js"></script>
-    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
-    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js"></script>
-	<script type="text/javascript" src="/wp-content/themes/charis/public/scripts/jquery.datetimepicker.js"></script>
-
-    <script src="/wp-content/themes/charis/public/scripts/scripts.js"></script>
-    <script src="/wp-content/themes/charis/public/scripts/bootstrap.min.js"></script>
-</body>
</html>
