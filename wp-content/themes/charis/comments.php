<?php if ( 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) return; ?>
<div class="bootstrap">
	<div class="panel top-space">
		<section id="comments">
			<?php 
			if ( have_comments() ) : 
			global $comments_by_type;
			$comments_by_type = &separate_comments( $comments );
			if ( ! empty( $comments_by_type['comment'] ) ) : 
			?>
			<section id="comments-list" class="comments">
			<h3 class="comments-title"><?php comments_number(); ?></h3>
			<?php if ( get_comment_pages_count() > 1 ) : ?>
			<nav id="comments-nav-above" class="comments-navigation" role="navigation">
			<div class="paginated-comments-links"><?php paginate_comments_links(); ?></div>
			</nav>
			<?php endif; ?>
			<ul class="commentList">
			<?php wp_list_comments( 'type=comment' ); ?>
			</ul>
			<?php if ( get_comment_pages_count() > 1 ) : ?>
			<nav id="comments-nav-below" class="comments-navigation" role="navigation">
			<div class="paginated-comments-links"><?php paginate_comments_links(); ?></div>
			</nav>
			<?php endif; ?>
			</section>
			<?php 
			endif; 
			if ( ! empty( $comments_by_type['pings'] ) ) : 
			$ping_count = count( $comments_by_type['pings'] ); 
			?>
			<section id="trackbacks-list" class="comments">
			<h3 class="comments-title"><?php echo '<span class="ping-count">' . $ping_count . '</span> ' . ( $ping_count > 1 ? __( 'Trackbacks', 'CHARIS' ) : __( 'Trackback', 'CHARIS' ) ); ?></h3>
			<ul>
			<?php wp_list_comments( 'type=pings&callback=CHARIS_custom_pings' ); ?>
			</ul>
			</section>
			<?php 
			endif; 
			endif;
			?>
		</section>
	</div>
</div>
<?php
$fields =  array(
	'author' => '<div class="noPad col-50"><p class="comment-form-author">' . '<label class="noPad col-100" for="author">' . __( 'Name' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label> ' .
	            '<input id="author" name="author" class="noPad col-100" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',
	'email'  => '<div class="col-50"><p class="comment-form-email"><label class="noPad col-100" for="email">' . __( 'Email' ) . ( $req ? '<span class="required">*</span>' : '' ) .  '</label> ' .
	            '<input id="email" name="email" class="noPad col-100" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',
	'url'	=>	''
); 

$args = array(
  'id_form'           => 'commentform',
  'id_submit'         => 'submit',
  'class_submit'      => 'submit',
  'name_submit'       => 'submit',
  'title_reply'       => __( 'Leave a Reply' ),
  'title_reply_to'    => __( 'Leave a Reply to %s' ),
  'cancel_reply_link' => __( 'Cancel Reply' ),
  'label_submit'      => __( 'Post Comment' ),
  'format'            => 'xhtml',
  'comment_notes_after' => '',
  'fields' => $fields
);

if ( comments_open() ) ?>
<div class="bootstrap">
	<div class="panel top-space">
		<?php comment_form($args); ?>
	</div>
</div>
