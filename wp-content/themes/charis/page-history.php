<?php
/*
Template Name: Category Page
*/
get_header(); 
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<div class="hero" style="background-image: url('<?php echo $url; ?>')"/>
	
</div>
<section class="panel nolines">
	<div class="container">
		<div class="grid feat-posts int-feat">
			<div class="col-100 rightpad">
				<h3 class="thin ttle"><?php echo get_the_title(); ?> · Featured Posts</h3>
			</div>
			<div class="col-50 ver">
				<?php

				  $args=array(
				    'post_type' => 'post',
				    'category__and' => array( get_field("category_select") , 2) ,
				    'offset' => '0',
				    'posts_per_page' => 1
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  
				  if( $my_query->have_posts() ) {
				    while ($my_query->have_posts()) : $my_query->the_post(); 
						$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
					    <div class="post">
							<a href="<?php the_permalink() ?>"><div class="img" style="background: url('<?php echo $url; ?>') center/cover"></div></a>
							<span>
								<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
								<p><?php the_excerpt(); ?></p>
								Posted by <?php the_author_nickname(); ?>
							</span>
						</div>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;
				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>
			</div>
			<div class="col-50 hoz">
				<?php
				  $args=array(
				    'post_type' => 'post',
				    'category__and' => array(get_field("category_select") , 2) ,
				    'offset' => '1',
				    'posts_per_page' => 2
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  
				  if( $my_query->have_posts() ) {
				    while ($my_query->have_posts()) : $my_query->the_post(); 
						$url = wp_get_attachment_thumb_url( get_post_thumbnail_id($post->ID) ); ?>
					    <div class="post">
							<a href="<?php the_permalink() ?>"><div class="img" style="background: url('<?php echo $url; ?>') center/cover"></div></a>
							<span>
								<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
								<p><?php the_excerpt(); ?></p>
Posted by <?php the_author_nickname(); ?>
							</span>
						</div>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;
				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>
			</div>
<!--			<hr class="col-100" />		-->
				<div class="grid" style="text-align: center; margin-top: 0;">
					<a href="/category/<?php echo get_the_title(); ?>" class="btn-outline col-50" style="margin: 0 auto;">See all</a>
				</div>
		</div>
	</div> 
</section>
<section class="panel white">
	<div class="container">
		<div class="grid">
			<div class="col-100">
				<div class="ad wide">
							<?php
							  $args=array(
							    'post_type' => 'ad',
							    'cat' => 9,
							    'post_status' => 'publish',
							    'posts_per_page' => 1
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post();
			
							    
							    ?>
									<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
			
							}
							
							  
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
											    										
						</div>
			
			
<!--				<img src="http://placehold.it/1000x150" width="100%" />-->
			</div>
		</div>
	</div>
</section>
<section class="panel secondary">
	<div class="container">
	<h2>OTHER RESOURCES</h2>
		<div class="grid carou">

		<?php
				  $args=array(
				    'post_type' => 'resource',
				    'cat' => get_field("category_select"),
				    'offset' => '0',
				    'posts_per_page' => 16
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  
				  if( $my_query->have_posts() ) {
				    while ($my_query->have_posts()) : $my_query->the_post(); 
					$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<div>
							<div class="carou-post">
								<h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
								<!--<img src="<?php echo $url; ?>"-->
								<div style="background-color: <?php the_field('primary_image_color'); ?>; background-image: url('<?php echo $url; ?>');" class="theimg"></div>
								<p><?php the_excerpt(); ?></p>
							</div>
						</div>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;
				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>	
		</div>
	</div>
</section>
<?php get_footer(); ?>