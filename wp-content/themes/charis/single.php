		<?php 	get_header(); 
		if ( have_posts() ) : while ( have_posts() ) : the_post();
		$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<section class="panel hero" style="background-image: url('<?php echo $url; ?>')">

</section>
<section class="panel first">
	<div class="container">
		<div class="grid">
			<h2 class="col-100"><?php the_title(); ?></h2>
			<article class="col-100">
				<!--<?php get_template_part( 'entry' ); ?>-->
				<?php the_content(); ?>
				<div class="bootstrap">
					<div class="panel top-space">
						<div id="social">
							<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><img src="/wp-content/themes/charis/public/images/Icon-Facebook.svg" alt="Share on Facebook" /> Share</a>
							<a target="_blank" href="http://twitter.com/share?text=<?php wp_title(''); ?>&url=<?php echo get_permalink(); ?>"><img src="/wp-content/themes/charis/public/images/Icon-Twitter.svg" alt="Tweet" /> Tweet</a>
						</div>
						<h5>Post Info:  </h5>
						<span class="btn btn-info btn-sm">
								<strong>Author:</strong> <?php the_author(); ?>
						</span>
						<span class="btn btn-info btn-sm">
								<strong>Publish Date:</strong>  <?php the_time('F j, Y'); ?>
						</span>
	
					</div>
					<?php if( has_category('conversations') ): ?>
						<?php comments_template(); ?> 
					<?php endif; ?>
				</div>
			</article>
		</div>
	</div>
</section>
			<?php endwhile; endif; ?>

<?php get_footer(); ?>