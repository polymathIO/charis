<html lang="en">
  <head>
    <meta charset="utf-8">

<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="icon" type="image/png" href="/favicon.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/wp-content/themes/charis/public/scripts/prefixfree.min.js"></script>

    <link href="/wp-content/themes/charis/public/styles/styles.css" rel="stylesheet" type="text/css" media="all"/>

    <!--[if lt IE 9]>
      <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
      </script>

      <link href="//wp-content/themes/charis/public/styles/ie.min.css" rel="stylesheet" type="text/css" media="all"/>
    <![endif]-->

    <!-- TODO: Complete this project :) -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/charis/public/scripts/jquery.datetimepicker.css"/ >

	<?php wp_head(); ?>      
  </head>
  <body>
    
    
<nav class="mainnav" >
	<a href="/" class="logo">
		<img src="/wp-content/themes/charis/public/images/CHARIS-Logos-Proxima.png" />
	</a>
	<div  id="slideDown" class="hidden" >
		<ul class="flyout-nav">
			
			<li>
			<a href="/conversations">Conversations</a>
					<ul class="megamenu">
						<div class="scrolly">
							<?php
							  $args=array(
							    'post_type' => 'post',
							    'offset' => '0',
							    'category_name' => 'conversations'
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post(); 
									$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<div>
										<a href="<?php the_permalink() ?>" style="background-image: url('<?php echo $url; ?>')">
											<h5><?php the_title(); ?></h5>
										</a>
									</div>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
							  }
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
						</div>
					</ul>
				</li>
			<li>
					<a href="/history">History</a>
					<ul class="megamenu">
						<div class="scrolly">
							<?php
							  $args=array(
							    'post_type' => 'post',
							    'offset' => '0',
							    'category_name' => 'history'
							    
							    
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post(); 
									$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<div>
										<a href="<?php the_permalink() ?>" style="background-image: url('<?php echo $url; ?>')">
											<h5><?php the_title(); ?></h5>
										</a>
									</div>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
							  }
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>	
						</div>
					</ul>
				</li>
			<li>
				<a href="/church">Church</a>
				<ul class="megamenu">
						<div class="scrolly">
							<?php
							  $args=array(
							    'post_type' => 'post',
							    'offset' => '0',
							    'category_name' => 'church'
							    
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post(); 
									$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<div>
										<a href="<?php the_permalink() ?>" style="background-image: url('<?php echo $url; ?>')">
											<h5><?php the_title(); ?></h5>
										</a>
									</div>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
							  }
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>	
						</div>
				</ul>
			</li>
			<li>
				<a href="/university">University</a>
				<ul class="megamenu">
						<div class="scrolly">
							<?php
							  $args=array(
							    'post_type' => 'post',
							    'offset' => '0',
							    'category_name' => 'university'
							    
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post(); 
									$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<div>
										<a href="<?php the_permalink() ?>" style="background-image: url('<?php echo $url; ?>')">
											<h5><?php the_title(); ?></h5>
										</a>
									</div>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
							  }
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>	
						</div>
				</ul>
			</li>
			<li>
				<a href="/search">Sources</a>	
			</li>
			<li>
				<a href="/about">About</a>	
			</li>
			<li class="mobileSearch">
				<form role="search" method="get" class="mobile-search-form" action="<?php echo home_url( '/' ); ?>">
					<input type="text" type="search" class="mobile-search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" id="tags" />
					<input style="display: none;" type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
				</form>
				<a href="/search">Advanced Search</a>
			</li>
				
		</ul>
	</div>
	<button id="hamb">
	    <div id="rect1" class="showRect"></div> 
	    <div id="rect2" class="showRect"></div> 
	    <div id="rect3" class="showRect"></div> 
	</button>
<!--<div id="hamburger">
<!--<a id="nav-toggle" href="#"><span></span></a>--*>
</div>-->
<div class="search-top">
	<?php get_search_form(); ?>
	<div class="dropdown">
		<div class="search-results">
			<ul class="prompt">
				<li>Start typing to see results...</li>
			</ul>
		</div>
		<a href="/search">Advanced Search</a>
	</div>
</div>
</nav>