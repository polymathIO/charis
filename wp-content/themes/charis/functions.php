<?php
add_action( 'after_setup_theme', 'CHARIS_setup' );
function CHARIS_setup()
{
load_theme_textdomain( 'CHARIS', get_template_directory() . '/languages' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'CHARIS' ) )
);
}
add_action( 'wp_enqueue_scripts', 'CHARIS_load_scripts' );
function CHARIS_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'CHARIS_enqueue_comment_reply_script' );
function CHARIS_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'CHARIS_title' );
function CHARIS_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'CHARIS_filter_wp_title' );
function CHARIS_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function next_posts_link_attributes() {
    return 'class="btn btn-info btn-sm alignleft"';
}
function prev_posts_link_attributes() {
    return 'class="btn btn-info btn-sm alignright"';
}

add_action( 'widgets_init', 'CHARIS_widgets_init' );
function CHARIS_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'CHARIS' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function CHARIS_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'CHARIS_comments_number' );
function CHARIS_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// Replaces the excerpt "[…]" read-more text with a link
function new_excerpt_more($more) {
       global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '">&nbsp;[…]</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');