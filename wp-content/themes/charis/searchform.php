<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<i class="icon-search"></i>
	<?php $search_box_term = is_search() ? get_search_query() : 'Website Search'; ?>
	<?php echo is_search(); ?>
	<input type="text" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Website Search …', 'placeholder' ) ?>" value="<?php echo $search_box_term; ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" id="tags" />
	<input style="display: none;" type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>