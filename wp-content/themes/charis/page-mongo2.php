<?php
/*
Template Name: Mongo2
*/

// ----------------  Connect to DB ---------------------  //

//$dsn = "mongodb://54.201.26.69";
//$client = new Client($dsn);
//$database = $client->getDatabase('charis');
//$collection = $database->getCollection('Metadata');

// Configuration
//$dbhost = 'ec2-54-68-154-24.us-west-2.compute.amazonaws.com';

$dbhost = 'ec2-54-68-83-204.us-west-2.compute.amazonaws.com';
$dbname = 'charis';

// Connect to charis database
$mongo = new MongoClient("mongodb://$dbhost");
$db = $mongo->$dbname;

// Get the metadata collection
$metadata = $db->Metadata;
//$metadata->ensureIndex( array("subject"=> "text" ) );

// ---------------- Get URL Parameters ---------------------  //


// -------------------- Set Query -----------------------  //
function cnsl($data) {
    if(is_array($data) || is_object($data))
	{
		echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.log('PHP: ".$data."');</script>");
	}
}

function is_set($var) {
    if(isset($var) && $var != null) {
		return true;
	} else {
		return false;
	}
}

function fuzzyKeyword($user_query) {
	$user_query = preg_replace("/[[:blank:]]+/"," ", $user_query);
	$arr_query = explode(' ', $user_query);

	if (count($arr_query) > 1) {
		cnsl('array');
	    $tmp = array();

	    foreach ($arr_query as $q) {
	        $tmp[] = new MongoRegex( "/". $q ."/i" );
	    }

	    $keyword = array('$in' => $tmp);
	    cnsl($keyword);

	} else {
		cnsl('single');
	    $keyword = new MongoRegex( "/". $user_query ."/i" );
	    cnsl($keyword);
	}
	return $keyword;
}

if (is_set($_GET['limit'])) {
	$limit = $_GET['limit'];
} else {
	$limit = 25;
}

if (is_set($_GET['pagenum'])) {
	$page = $_GET['pagenum'];
} else {
	$_GET['pagenum'] = 1;
	$page = 1;
}

$skip = ($page * $limit) - $limit;


$acc = array(
    '$and' => array(
    ),
    '$or'  => array(
    ),
    '$nor'  => array(
    )
);

if (is_set($_GET['Keyword'])) {
	$Keyword = $_GET['Keyword'][0];
	/*$acc['Keyword'] = $Keyword;*/
	$acc['$text'] = array( '$search' => "".addslashes($Keyword)."", '$language' => 'en');
	cnsl($Keyword);
	cnsl(addslashes($Keyword));
}

$oai_dc = [ 'title','creator','subject','description','publisher','contributor','format','identifier','source','language','relation','coverage','rights'];






foreach ($oai_dc as $keyword) {
	if (is_set($_GET['and_'.$keyword])) {
		foreach($_GET['and_'.$keyword] as $val) {
			array_push($acc['$and'], array($keyword => fuzzyKeyword($val)));
		}
	}
	if (is_set($_GET['or_'.$keyword])) {
		foreach($_GET['or_'.$keyword] as $val) {
			array_push($acc['$or'], array($keyword => fuzzyKeyword($val)));
		}
	}
	if (is_set($_GET['not_'.$keyword])) {
		foreach($_GET['not_'.$keyword] as $val) {
			array_push($acc['$nor'], array($keyword => fuzzyKeyword($val)));
		}
	}
}

if (is_set($_GET['type'])) {
	$type = $_GET['type'];
	$acc['type'] = array('$in' => $type);
}

if (is_set($_GET['sortBy'])) {
	$sortBy = $_GET['sortBy'];
} else {
	$sortBy = 'date';
}
if (is_set($_GET['sortDir'])) {
	$sortDir = intval($_GET['sortDir']);
} else {
	$sortDir = 1;
}
$sort = array($sortBy => $sortDir);

if ( is_set($_GET['before']) && is_set($_GET['after']) ) {
	echo('both');
	$start = new MongoDate(strtotime($_GET['after']));
	$end = new MongoDate(strtotime($_GET['before']));

	$acc['date'] = array('$gte' => $start, '$lte' => $end);

} else if (is_set($_GET['before'])) {
	echo('before');
	$before = new MongoDate(strtotime($_GET['before']));
	echo($before);
	$acc['date'] = array( '$lte' => $before );
} else if (is_set($_GET['after'])) {
	echo('after');
	$after = new MongoDate(strtotime($_GET['after']));
	$acc['date'] = array( '$gte' => $after );
} 

//var_dump($acc);


if(empty($acc['$and'])) {
	unset($acc['$and']);
}
if(empty($acc['$or'])) {
	unset($acc['$or']);
}
if(empty($acc['$nor'])) {
	unset($acc['$nor']);
}

$records = $metadata->find($acc)->limit($limit)->sort($sort)->skip($skip);
$numOfRecords = $records->count();
$totalPageNumber = $numOfRecords / $limit;
//$totalPageNumber = 1200;
cnsl($totalPageNumber);

function paginateLink($diff) {
	global $page;
	$newPage = $diff + $page;
	if ( $newPage < 0 || $newPage > $totalPageNumber ) {
		cnsl($diff);
		cnsl($page);
		return;
	} else  {
		$params = $_GET;
		cnsl($_GET);
		$params['pagenum'] = $newPage;
		$paramString = http_build_query($params);
		echo('<a href="./"'.$paramString.'" >'. $newPage .'</a>');
	}
}

function paginateLinkFun($amount)
{
	if ($amount == 'next') {
		$newPage = $_GET['pagenum'] + 1;
		echo '<a href="'.$myurl.'&pagenum='.$newPage.'">&raquo;</a>';
	} else if ($amount == 'prev') {
		$newPage = $_GET['pagenum'] - 1;
		echo '<a href="'.$myurl.'&pagenum='.$newPage.'">&laquo;</a>';
	} else {
		parse_str($_SERVER['QUERY_STRING'], $parse);
		$parse['pagenum'] = $newPage;

		$newPage = $_GET['pagenum'] + $amount;
		$params = http_build_query($_GET);
		$myurl = basename($_SERVER['PHP_SELF']) . "?" . $params;

		echo '<a href="'.preg_replace( '/pageNum\=\d+/' , 'pageNum='.$newPage , $myurl).'&pagenum='.$newPage.'">'.$newPage.'</a>';
	}
}

function sortingLink($by, $dir)
{
	$myurl = basename($_SERVER['PHP_SELF']) . "?" . $_SERVER['QUERY_STRING'];
	echo '<a role="menuitem" tabindex="-1" href="'.preg_replace( '/&sortDir=[-1-2]+/', '', preg_replace( '/&sortBy=\w+/' , '' , $myurl)).'&sortBy='.$by.'&sortDir='.$dir.'">';
}

get_header(); 

if (is_set($_GET['Keyword'])) {
	echo('<script>var textSearch = "'.$Keyword.'"</script>');
}

?>

<section class="panel first">
	<div class="container">
		<div class="grid feat-posts">

			<div class="bootstrap col-100" style="margin-top: 3rem;">
				<div align="center">Displaying <?php echo $skip + 1; ?> - <?php if($limit + $skip > $numOfRecords){ echo $numOfRecords; } else { echo $skip + $limit; } ?> of <?php echo $numOfRecords; ?> results</div>
				<a href="/search/" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Back to Advanced Search Page</a>
				<span class="dropdown pull-right">
				  <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
				    Sort By: <?php echo ucwords($sortBy); ?>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
				    <?php if ( $sortBy == 'title'): ?>
				    	<li><?php sortingLink('title', 0 - $sortDir); ?><strong>Title</strong></a></li>
				    <?php else: ?>
				    	<li><?php sortingLink('title', $sortDir); ?>Title</a></li>
				    <?php endif; ?>
   				    <?php if ( $sortBy == 'date'): ?>
				    	<li><?php sortingLink('date', 0 - $sortDir); ?><strong>Date</strong></a></li>
				    <?php else: ?>
				    	<li><?php sortingLink('date', $sortDir); ?> Date</a></li>
				    <?php endif; ?>
				  </ul>
				</span>
			</div>
			<h2 class="col-100">Search Results</h2>


			<?php foreach($records as $document) { 
				$md = $document;  ?>
				
				<div class="result col-100">
					<h4 class="thin title">
						<?php if (preg_match("/^http:\/\//", $md['description'][1])) { 
						cnsl('1');
						?>
							<a target="_blank" href="<?php echo $md['description'][1]; ?>">
						<?php } ?>
						<?php if (preg_match("/^http:\/\//", $md['identifier'][1])) { 
						cnsl('1');
						?>
							<a target="_blank" href="<?php echo $md['identifier'][1]; ?>">
						<?php } ?>
						<?php if (preg_match("/^http:\/\//", $md['identifier'][0])) { 
						cnsl('0');
						?>
							<a target="_blank" href="<?php echo $md['identifier'][0]; ?>">
						<?php } ?>
						<?php if (preg_match("/^http:\/\/texashistory/", $md['identifier'][2])) { 
							cnsl('2');
						?>
							<a target="_blank" href="<?php echo $md['identifier'][2]; ?>">
						<?php } ?>
						<?php if (preg_match("/^ark:/", $md['identifier'][2])) { 
							cnsl('3');
						?>
							<a target="_blank" href="http://texashistory.unt.edu/<?php echo substr($md['identifier'][2], 5); ?>">
						<?php } ?>
							<?php if (gettype($md['title']) == "array") {
								echo('<span class="bold-search-terms">'.$md['title'][0].'</span>');
							} else {
								echo('<span class="bold-search-terms">'.$md['title'].'</span>');							
							} ?>
						</a>
					</h4>
					<?php if (preg_match("/^http:\/\//", $md['description'][1])) { 
						cnsl('1');
					?>
						<img src="<?php echo $md['description'][1]; ?>" />
					<?php } ?>
					<?php if (preg_match("/^http:\/\/texashistory/", $md['identifier'][2])) { 
						cnsl('2');
					?>
						<img src="<?php echo $md['identifier'][2]; ?>small" />
					<?php } ?>
					<?php if (preg_match("/^ark:/", $md['identifier'][2])) { 
						cnsl('3');
					?>
						<img src="http://texashistory.unt.edu/<?php echo substr($md['identifier'][2], 5); ?>/small" />
					<?php } ?>
					<div class="res-body" >
						<p class="bold-search-terms">
							<?php if (gettype($md['description']) == "array") {
							    echo $md['description'][0];							
							} else {
						    	echo $md['description'];
						    } ?>						
						</p>
						<div class="bootstrap bold-search-terms">
							
							<div class="panel top-space"><h5>Meta:  </h5>
								<span class="btn btn-info btn-sm">
									<strong>Date:</strong> <?php echo date('Y-m-d', $md['date']->sec); ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Type:</strong> 
									<?php if (gettype($md['type']) == "array") {
										foreach($md['type'] as $type) { ?>
									    <?php echo $type; ?>
									<?php }
									} else { ?>
								    	<?php echo $md['type']; ?>
								    <?php } ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Language:</strong>
									<?php if (gettype($md['language']) == "array") {
										foreach($md['language'] as $language) { ?>
									    <?php echo $language; ?>
									<?php }
									} else { ?>
								    	<?php echo $md['language']; ?>
								    <?php } ?>
								</span>
							</div>
							<?php if ($md['creator']): ?>
								<div class="panel top-space"><h5>Creators:  </h5>
									
									<?php if (gettype($md['creator']) == "array") {
										foreach($md['creator'] as $creator) { ?>
									    <span class="btn btn-info btn-sm"><?php echo $creator; ?></span>
									<?php }
									} else { ?>
								    	<span class="btn btn-info btn-sm"><?php echo $md['creator']; ?></span>
								    <?php } ?>
								</div>
							<?php endif; ?>
							<div class="panel top-space"><h5>Subjects:  </h5>
								
								<?php if (gettype($md['subject']) == "array") {
									foreach($md['subject'] as $subject) { ?>
								    <span class="btn btn-info btn-sm"><?php echo $subject; ?></span>
								<?php }
								} else { ?>
							    	<span class="btn btn-info btn-sm"><?php echo $md['subject']; ?></span>
							    <?php } ?>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>
			

		</div>
	</div>
	<div class="bootstrap" align="center">
		<ul class="pagination">
		  	 <?php if (is_set($_GET['pagenum'])): ?>
			  	<li>
			  			<?php paginateLinkFun('prev'); ?>
		  		</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] > 2): ?>
			  	<li>
			  		<?php paginateLinkFun(-2); ?>
		  		</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] > 1): ?>
			  	<li>
				  		<?php paginateLinkFun(-1); ?>
	  			</li>
		  	<?php endif; ?>
		  	<li class="active">
		  		<a href="#">
		  			<?php echo $_GET['pagenum']; ?>
		  		</a>
		  	</li>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber ): ?>
			  	<li>
				  		<?php paginateLinkFun(1); ?>
	  			</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber - 1 ): ?>
			  	<li>
				  		<?php paginateLinkFun(2); ?>
	  			</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber ): ?>
			  	<li>
				  		<?php paginateLinkFun('next'); ?>
	  			</li>
		  	<?php endif; ?>
		</ul>
	</div>
</section>
<section class="panel white">
	<div class="container">
		<div class="grid">
			<h2 class="col-100">About CHARIS</h2>
			<div class="col-66">
				<p class="rightpad">
					CHARIS hosts conversations of and about Churches of Christ.  The website is intended to support education for Christian life and community through contemporary discussions and historical sources that variously witness to the gifts (“charis”) of God among Churches of Christ, especially their plea for visible unity among Christians through ongoing renewal and restoration of Scriptural beliefs and practices among God’s people.
				</p>
				<p class="rightpad">
					The CHARIS website is supported by Abilene Christian University (Abilene, TX, USA), the Center for Heritage and Renewal in Spirituality (CHARIS) at ACU. The purpose of CHARIS at ACU is to seek God’s blessings for a healthy relationship between the Christian college/university – its faculty, staff, and students – and the church heritage that gives identity and meaning to such a school.  This underlying concern for Christian colleges/universities, and their relationship to the churches, is reflected in the form and content of the CHARIS website. 
				</p>
				<h3 class="title thin">Contact Us</h3>
 
				<?php echo do_shortcode( '[contact-form-7 id="9" title="Contact form"]' ); ?>
				
				
			</div>
			<div class="col-33">
				<div class="ad">
							<?php
							  $args=array(
							    'post_type' => 'ad',
							    'post_status' => 'publish',
							    'posts_per_page' => 1
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post();
			
							    
							    ?>
									<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
			
							}
							
							  
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
											    										
						</div>
			
			</div>
		</div>
	</div>
</section>
-	
-
-
-    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
-    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
-    <script type="text/javascript" src="//use.typekit.net/bst0pmp.js"></script>
-    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
-    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js"></script>
-
-    <script src="/wp-content/themes/charis/public/scripts/scripts.js"></script>
-    <script src="/wp-content/themes/charis/public/scripts/bootstrap.min.js"></script>
