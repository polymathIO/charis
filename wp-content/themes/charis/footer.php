
<section class="panel white">
	<div class="container">
		<div class="grid">
			<h2 class="col-100">About CHARIS</h2>
			<div class="col-66">
				<p class="rightpad">
					CHARIS hosts conversations of and about Churches of Christ.  The website is intended to support education for Christian life and community through contemporary discussions and historical sources that variously witness to the gifts (“charis”) of God among Churches of Christ, especially their plea for visible unity among Christians through ongoing renewal and restoration of Scriptural beliefs and practices among God’s people.
				</p>
				<p class="rightpad">
					The CHARIS website is supported by Abilene Christian University (Abilene, TX, USA), the Center for Heritage and Renewal in Spirituality (CHARIS) at ACU. The purpose of CHARIS at ACU is to seek God’s blessings for a healthy relationship between the Christian college/university – its faculty, staff, and students – and the church heritage that gives identity and meaning to such a school.  This underlying concern for Christian colleges/universities, and their relationship to the churches, is reflected in the form and content of the CHARIS website. 
				</p>
				<h3 class="title thin">Contact Us</h3>
 
				<?php echo do_shortcode( '[contact-form-7 id="9" title="Contact form"]' ); ?>
				
				
			</div>
			<div class="col-33">
							<?php
							  $args=array(
							    'post_type' => 'ad',
							    'cat' => 10,
							    'post_status' => 'publish',
							    'posts_per_page' => 1
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post();
			
							    
							    ?>
				<div class="ad square">
							    
									<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
				</div>
				
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
			
							}
							
							  
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
											    										
			
			</div>
			<div class="row">
				<div class="copyright col-75">&copy;2014-2015 Abilene Christian University and all rights reserved unless otherwise noted.</div>
				<div class="socialaccounts col-25">
					CHARIS <a target="_blank" href="https://www.facebook.com/chariswebsite"><img src="/wp-content/themes/charis/public/images/Icon-Facebook.svg" alt="CHARIS on Facebook" /></a>
					<a target="_blank" href="https://twitter.com/chariswebsite"><img src="/wp-content/themes/charis/public/images/Icon-Twitter.svg" alt="CHARIS on Twitter" /></a>
				</div>
			</div>
		</div>
	</div>
</section>
	

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script type="text/javascript" src="//use.typekit.net/bst0pmp.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js"></script>

    <script src="/wp-content/themes/charis/public/scripts/scripts.js"></script>
    <script src="/wp-content/themes/charis/public/scripts/bootstrap.min.js"></script>
  	<?php wp_footer(); ?>
  </body>
</html>