<?php
/*
Template Name: resources

*/
get_header(); ?>
<section class="panel first">
	<div class="container">
		<div class="grid">
			<?php
				  $args=array(
				    'post_type' => 'resource',
				    'post_status' => 'publish'
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  if( $my_query->have_posts() ) :
				  while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="result col-100">
					<h4 class="thin title">
						<a target="_blank" href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h4>
					<div class="res-body" >
						<p><?php the_excerpt(); ?></p>
						<div class="bootstrap">
							<div class="panel top-space"><h5>Post Info:  </h5>
								<span class="btn btn-info btn-sm">
									<strong>Author:</strong> <?php the_author(); ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Publish Date:</strong>  <?php the_time('F j, Y'); ?>
								</span>
							</div>
						</div>
					</div>
				</div>
			<?php 	endwhile; 
					endif;
					wp_reset_query();
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>