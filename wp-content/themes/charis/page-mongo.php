<?php
/*
Template Name: Mongo
*/

require 'vendor/autoload.php';

use \Sokil\Mongo\Client;
use \Sokil\Mongo\Database;
use \Sokil\Mongo\Collection;
use \Sokil\Mongo\Document;
use \Sokil\Mongo\QueryBuilder;
use \Sokil\Mongo\AggregatePipelines;
use \Sokil\Mongo\Behavior;
use \Sokil\Mongo\Client;
use \Sokil\Mongo\ClientPool;
use \Sokil\Mongo\Collection;
use \Sokil\Mongo\Cursor;
use \Sokil\Mongo\Database;
use \Sokil\Mongo\Document;
use \Sokil\Mongo\Event;
use \Sokil\Mongo\Exception;
use \Sokil\Mongo\Expression;
use \Sokil\Mongo\GridFS;
use \Sokil\Mongo\GridFSFile;
use \Sokil\Mongo\GridFSQueryBuilder;
use \Sokil\Mongo\Operator;
use \Sokil\Mongo\Paginator;
use \Sokil\Mongo\Persistence;
use \Sokil\Mongo\QueryBuilder;
use \Sokil\Mongo\Queue;
use \Sokil\Mongo\Structure;
use \Sokil\Mongo\Validator;
// ----------------  Connect to DB ---------------------  //

$dsn = "mongodb://54.201.26.69"; 
$client = new Client($dsn);
$database = $client->getDatabase('charis');
$collection = $database->getCollection('Metadata');

// ---------------- Get URL Parameters ---------------------  //


// -------------------- Set Query -----------------------  //

if (isset($_GET['limit'])) {
	$limit = $_GET['limit'];
} else {
	$limit = 25;
}

if (isset($_GET['pagenum'])) {
	$page = $_GET['pagenum'];
} else {
	$page = 1;
}

$cursor = $collection->find();

if (isset($_GET['title'])) {
	$title = $_GET['title'];
	$cursor = $cursor->whereIn('metadata.dc.title', $title);
}
if (isset($_GET['creator'])) {
	$creator = $_GET['creator'];
	$cursor = $cursor->whereIn('metadata.dc.creator', $creator);
}
if (isset($_GET['subject'])) {
	$subject = $_GET['subject'];
	$cursor = $cursor->whereIn('metadata.dc.subject', $subject);
}
if (isset($_GET['description'])) {
	$description = $_GET['description'];
	$cursor = $cursor->whereIn('metadata.dc.description', $description);
}
if (isset($_GET['publisher'])) {
	$publisher = $_GET['publisher'];
	$cursor = $cursor->whereIn('metadata.dc.publisher', $publisher);
}
if (isset($_GET['contributor'])) {
	$contributor = $_GET['contributor'];
	$cursor = $cursor->whereIn('metadata.dc.contributor', $contributor);
}
if (isset($_GET['date'])) {
	$date = $_GET['date'];
	$cursor = $cursor->whereIn('metadata.dc.date', $date);
}
if (isset($_GET['type'])) {
	$type = $_GET['type'];
	$cursor = $cursor->whereIn('metadata.dc.type', $type);
}
if (isset($_GET['format'])) {
	$format = $_GET['format'];
	$cursor = $cursor->whereIn('metadata.dc.format', $format);
}
if (isset($_GET['identifier'])) {
	$identifier = $_GET['identifier'];
	$cursor = $cursor->whereIn('metadata.dc.identifier', $identifier);
}
if (isset($_GET['source'])) {
	$source = $_GET['source'];
	$cursor = $cursor->whereIn('metadata.dc.source', $source);
}
if (isset($_GET['language'])) {
	$language = $_GET['language'];
	$cursor = $cursor->whereIn('metadata.dc.language', $language);
}
if (isset($_GET['relation'])) {
	$relation = $_GET['relation'];
	$cursor = $cursor->whereIn('metadata.dc.relation', $relation);
}
if (isset($_GET['coverage'])) {
	$coverage = $_GET['coverage'];
	$cursor = $cursor->whereIn('metadata.dc.coverage', $coverage);
}
if (isset($_GET['rights'])) {
	$rights = $_GET['rights'];
	$cursor = $cursor->whereIn('metadata.dc.rights', $rights);
}

$paginator = $cursor->paginate($page, $limit);
$totalDocumentNumber = $paginator->getTotalRowsCount();
$totalPageNumber = $paginator->getTotalPagesCount();

function paginateLink($amount)
{
	$myurl = basename($_SERVER['PHP_SELF']) . "?" . $_SERVER['QUERY_STRING'];
	if ($amount == 'next') {
		$newPage = $_GET['pagenum'] + 1;
		echo '<a href="'.$myurl.'&pagenum='.$newPage.'">&raquo;</a>';
	} else if ($amount == 'prev') {
		$newPage = $_GET['pagenum'] - 1;
		echo '<a href="'.$myurl.'&pagenum='.$newPage.'">&laquo;</a>';
	} else {
		$newPage = $_GET['pagenum'] + $amount;
		echo '<a href="'.$myurl.'&pagenum='.$newPage.'">'.$newPage.'</a>';
	}
}

// ------------------ Display Results ---------------------  //

get_header(); ?>

<section class="panel first">
	<div class="container">
		<div class="grid feat-posts">
			<div class="bootstrap col-100">
				<a href="/search/" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Back to Advanced Search Page</a>
				<span style="display: none;" class="dropdown pull-right">
				  <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
				    Sort By: Relevance
				    <span class="caret"></span>
				  </button>
				  <ul  class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><strong>Relevance </strong><span class="pull-right glyphicon glyphicon-sort"></span></a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Title</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Author Name</a></li>
				    <li role="presentation" class="divider"></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Creation Date</a></li>
				    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Archive Date</a></li>
				  </ul>
				</span>
			</div>
			<h2 class="col-100">Search Results</h2>


			<?php foreach($paginator as $document) { 
				$metadata = $document->get('metadata.dc');  ?>
				
				<div class="result col-100">
					<h4 class="thin title">
						<a target="_blank" href="<?php echo $metadata['identifier'][1]; ?>"><?php echo $metadata['title'][0]; ?></a>
					</h4>
					<?php if (preg_match("/^http:\/\//", $metadata['identifier'][2])) { ?>
						<img src="<?php echo $metadata['identifier'][2]; ?>small" />
					<?php } ?>
					<?php if (preg_match("/^ark:/", $metadata['identifier'][2])) { ?>
						<img src="http://texashistory.unt.edu/<?php echo substr($metadata['identifier'][2], 5); ?>/small" />
					<?php } ?>
					<div class="res-body" >
						<p>
							<?php echo $metadata['description']; ?>
						</p>
						<div class="bootstrap">
							
							<div class="panel top-space"><h5>Meta:  </h5>
								<span class="btn btn-info btn-sm">
									<strong>Date:</strong> <?php echo $metadata['date']; ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Type:</strong> 
									<?php if (gettype($metadata['type']) == "array") {
										foreach($metadata['type'] as $type) { ?>
									    <?php echo $type; ?>
									<?php }
									} else { ?>
								    	<?php echo $metadata['type']; ?>
								    <?php } ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Language:</strong>
									<?php if (gettype($metadata['language']) == "array") {
										foreach($metadata['language'] as $language) { ?>
									    <?php echo $language; ?>
									<?php }
									} else { ?>
								    	<?php echo $metadata['language']; ?>
								    <?php } ?>
								</span>
							</div>
							<div class="panel top-space"><h5>Creators:  </h5>

								<?php if (gettype($metadata['creator']) == "array") {
									foreach($metadata['creator'] as $creator) { ?>
								    <span class="btn btn-info btn-sm"><?php echo $creator; ?></span>
								<?php }
								} else { ?>
							    	<span class="btn btn-info btn-sm"><?php echo $metadata['creator']; ?></span>
							    <?php } ?>
							</div>
							<div class="panel top-space"><h5>Subjects:  </h5>
								<?php if (gettype($metadata['subject']) == "array") {
									foreach($metadata['subject'] as $subject) { ?>
								    <span class="btn btn-info btn-sm"><?php echo $subject; ?></span>
								<?php }
								} else { ?>
							    	<span class="btn btn-info btn-sm"><?php echo $metadata['subject']; ?></span>
							    <?php } ?>
							</div>
						</div>
					</div>
				</div>

			<?php } ?>
			

		</div>
	</div>
	<div class="bootstrap" align="center">
		<ul class="pagination">
		  	<?php if (isset($_GET['pagenum'])): ?>
			  	<li>
			  			<?php paginateLink('prev'); ?>
		  		</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] > 2): ?>
			  	<li>
			  		<?php paginateLink(-2); ?>
		  		</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] > 1): ?>
			  	<li>
				  		<?php paginateLink(-1); ?>
	  			</li>
		  	<?php endif; ?>
		  	<li class="active">
		  		<a href="#">
		  			<?php echo $_GET['pagenum']; ?>
		  		</a>
		  	</li>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber ): ?>
			  	<li>
				  		<?php paginateLink(1); ?>
	  			</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber - 1 ): ?>
			  	<li>
				  		<?php paginateLink(2); ?>
	  			</li>
		  	<?php endif; ?>
		  	<?php if ($_GET['pagenum'] < $totalPageNumber ): ?>
			  	<li>
				  		<?php paginateLink('next'); ?>
	  			</li>
		  	<?php endif; ?>
		</ul>
	</div>
</section>
<section class="panel white">
	<div class="container">
		<div class="grid">
			<h2 class="col-100">About CHARIS</h2>
			<div class="col-66">
				<p class="rightpad">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
				<h3 class="title thin">Contact Us</h3>
				<form class="contact">
					<input type="text" placeholder="Name" />
					<input type="text" placeholder="Email" />
					<textarea>Message...</textarea>
					<input type="submit" />
				</form>
			</div>
			<div class="col-33">
				<img src="/wp-content/themes/charis/public/images/500x500.gif" />
			</div>
		</div>
	</div>
</section>
	


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script type="text/javascript" src="//use.typekit.net/bst0pmp.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js"></script>

    <script src="/wp-content/themes/charis/public/scripts/scripts.js"></script>
    <script src="/wp-content/themes/charis/public/scripts/bootstrap.min.js"></script>
  </body>
</html>    
