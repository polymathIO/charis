<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<!doctype html>
<?php if ( get_field('show_header_text') ) : ?>
<section class="messaging-header">
	<div>
		<h2><?php the_field('header_text'); ?></h2>
	</div>
</section>
<?php endif; ?>
<section class="panel first">
	<div class="container">
		<div class="grid feat-posts">
<!--			<div class="col-33 rightpad">
				<h3 class="thin title">Latest</h3>
			</div>
			<div class="col-33 rightpad">
				<h3 class="thin title">Featured</h3>
			</div>
			<div class="col-33 rightpad">
				<h3 class="thin title">Resources</h3>
			</div>-->

				<?php
				foreach( array(2, 28) as $index => $cat_id ) {
				  $ppp =array(3,3);
				  if ($cat_id == 2) {
					  $args=array(
					    'cat' => $cat_id,
					    'post_type' => 'post',
					    'post_status' => 'publish',
					    'posts_per_page' => $ppp[$index],
					    'caller_get_posts'=> 1
					  );
				  }
				  if ($cat_id == 28) {
					  $args=array(
					    'cat' => -28,
					    'post_type' => 'post',
					    'post_status' => 'publish',
					    'posts_per_page' => $ppp[$index],
					    'caller_get_posts'=> 1
					  );
				  }
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  if ($cat_id == 2) {
				  	$layout = "hoz";
				  	$heading = "Featured";
				  }
				  if ($cat_id == 28) {
				  	$layout = "ver";
				  	$heading = "Latest";
				  }
				  if( $my_query->have_posts() ) {
				    echo '<div class="col-33 '. $layout .'"> <div class="rightpad">
				    	<h3 class="thin title">'. $heading .'</h3>
				    </div>';
				    while ($my_query->have_posts()) : $my_query->the_post(); 
						if ($cat_id == 2) {
							$url = wp_get_attachment_thumb_url( get_post_thumbnail_id($post->ID) ); 
						} if ($cat_id == 28) {
							$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
						}
					?>

					    <div class="post thumb-<?php echo $url; ?> cat-<?php echo $cat_id; ?>">
							<div class="img" style="background: url('<?php echo $url; ?>') center/cover"></div>
							<span>
								<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
								<p><?php the_excerpt(); ?></p>
								<a href="/<?php $category = get_the_category(); echo $category[0]->cat_name; ?>">
									<?php $category = get_the_category(); echo $category[0]->cat_name; ?>
								</a> • by <?php the_author_nickname(); ?>
							</span>
						</div>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;
				    if ($cat_id == 2) {
				    	echo '<div class="rightpad">
				    	<a href="/category/featured/" class="btn-outline">See all</a>
				    	</div></div>';
					}
				    if ($cat_id == 28) {
				    	echo '<div class="rightpad">
				    	<a href="/archive/" class="btn-outline">See all</a>
				    	</div></div>';
					}

				  }
				}
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>
				<?php
				  $args=array(
				    'post_type' => 'resource',
				    'post_status' => 'publish',
				    'posts_per_page' => 3,
				    'caller_get_posts'=> 1
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  $layout = "mix";
				  $heading = "Resources";
				  if( $my_query->have_posts() ) {
				    echo '<div class="col-33 '. $layout .'"><div class="rightpad">
				    	<h3 class="thin title">'. $heading .'</h3>
				    </div>
				    ';
				    while ($my_query->have_posts()) : $my_query->the_post(); 						
				    $url = wp_get_attachment_thumb_url( get_post_thumbnail_id($post->ID) ); ?>
					    <div class="post">
							<div class="img" style="background: url('<?php echo $url; ?>') center/cover"></div>
							<span>
								<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
								<p><?php the_excerpt(); ?></p>
								<a href="/<?php
$category = get_the_category();
echo $category[0]->cat_name;
?>"><?php
$category = get_the_category();
echo $category[0]->cat_name;
?></a> • by <?php the_author_nickname(); ?>
							</span>
						</div>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;
				    echo '<div class="rightpad">
				    	<a href="/resource" class="btn-outline">See all</a>
				    </div></div>';

				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>
				<!--div class="col-33 rightpad">
					<a href="/archive" class="btn-outline">See all</a>
				</div>
				<div class="col-33 rightpad">
					<a href="/category/featured" class="btn-outline">See all</a>
				</div>
				<div class="col-33 rightpad">
					<a href="/resource" class="btn-outline">See all</a>
				</div-->		
	
			</div>	
		</div>
	</div>
</section>
<section class="panel white">
	<div class="container">
		<div class="grid">
			<div class="col-100">
				<div class="ad wide">
						<!--	<?php
							  $args=array(
							    'post_type' => 'ad',
							    'cat' => 9,
							    'post_status' => 'publish',
							    'posts_per_page' => 1
							  );
							  $my_query = null;
							  $my_query = new WP_Query($args);
							  
							  if( $my_query->have_posts() ) {
							    while ($my_query->have_posts()) : $my_query->the_post();
			
							    
							    ?>
									<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
							      <?php
							    //the_content();  //or the_excerpt{};
							    endwhile;
			
							}
							
							  
							wp_reset_query();  // Restore global post data stomped by the_post().
							?>
						-->
						<div id="bsap_1300967" class="bsarocks bsap_78a174b58d0080f2ef584290b5d5ece4"></div>
											    										
						</div>
			
			
			</div>
		</div>
	</div>
</section>

<section class="panel secondary">
	<div class="container">
		<div class="grid">
			<div class="col-33">
			<div class="ad square">
			
				<h3 class=" title thin">ADVERTISEMENT</h3>
				<!--<?php
				  $args=array(
				    'post_type' => 'ad',
				    'cat' => 10,
				    'post_status' => 'publish',
				    'posts_per_page' => 1
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  
				  if( $my_query->have_posts() ) {
				    while ($my_query->have_posts()) : $my_query->the_post();

				    
				    ?>
						<a href="<?php the_field("url"); ?>" target="_blank"><img src="<?php the_field("image"); ?>" alt="" /></a>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;

				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>-->
				<div id="bsap_1300968" class="bsarocks bsap_78a174b58d0080f2ef584290b5d5ece4"></div>
								    										
			</div>
			</div>
			<div class="col-66">
			<h3 class=" title thin">COMING UP</h3>
			<div class="events">
				<?php
				  $args=array(
				    'post_type' => 'event',
				    'post_status' => 'publish',
				    'posts_per_page' => 3,
				    'caller_get_posts'=> 1
				  );
				  $my_query = null;
				  $my_query = new WP_Query($args);
				  
				  if( $my_query->have_posts() ) {
				    while ($my_query->have_posts()) : $my_query->the_post();

				    $startDate = DateTime::createFromFormat('Ymd', get_field('startDate'));
				    $endDate = DateTime::createFromFormat('Ymd', get_field('endDate'));
				    ?>
						<div class="post">
							<a href="<?php the_permalink(); ?>"><div class="img" style='background: url("<?php the_field("image"); ?>") center/cover'></div>
							<span>
								<h3><?php the_title(); ?></h3>
								<p><?php echo $startDate->format('m/d'); ?> <?php if($endDate){ echo " - ".$endDate->format('m/d');} ?> • <?php the_field("location"); ?></p>
							</span>
						</div>
						</a>
				      <?php
				    //the_content();  //or the_excerpt{};
				    endwhile;

				  }
				wp_reset_query();  // Restore global post data stomped by the_post().
				?>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>