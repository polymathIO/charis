<?php
/*
Template Name: Leaf
*/
get_header(); ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); 
		$url = wp_get_attachment_url( get_post_thumbnail_id() );
	?>
<section class="panel hero leaf" style="background-image: url('<?php echo $url; ?>')">

</section>
<section class="panel first">
	<div class="container">
		<div class="grid">

				<h2 class="col-100"><?php the_title(); ?></h2>
				<article>
					<?php the_content(); ?>
				</article>
				<?php endwhile; else : ?>
					<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>