<?php get_header(); ?>

<?php 
	// vars
	$queried_object = get_queried_object(); 
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;  

	// load thumbnail for this taxonomy term (term object)

	$is_blog = get_field('is_this_a_blog', $queried_object);
	$hero = get_field('hero_image', $queried_object);
	$author = get_field('author', $queried_object);

?>

<?php if ( $is_blog ) : ?>
	<div class="hero" style="background-image: url('<?php echo $hero; ?>')"/>
		<h1>
			<?php
				if ( is_category() ) {
					global $cat;
					$curr_cat = get_category( $cat );
					$cat_name = ( $curr_cat ) ? $curr_cat->cat_name : 'No Category Found!!';
					echo $cat_name;
				}
			?>
		</h1>
	</div>
<?php endif; ?>
<section class="panel first">
	<div class="container">
		<div class="grid">
			<?php if (have_posts()) : while (have_posts()) : the_post();?>
				<div class="result col-100">
					<h4 class="thin title">
						<a target="_blank" href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h4>
					<div class="res-body" >
						<p><?php the_excerpt(); ?></p>
						<div class="bootstrap">
							<div class="panel top-space"><h5>Post Info:  </h5>
								<span class="btn btn-info btn-sm">
									<strong>Author:</strong> <?php the_author(); ?>
								</span>
								<span class="btn btn-info btn-sm">
									<strong>Publish Date:</strong>  <?php the_time('F j, Y'); ?>
								</span>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
				<div class="col-100 bootstrap">
					<?php next_posts_link( '&laquo; Older posts' ); ?>
					<?php previous_posts_link( 'Newer posts &raquo;' ); ?>
				</div>
			<?php else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>