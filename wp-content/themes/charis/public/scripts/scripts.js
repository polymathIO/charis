

var hamburger="hamb"; //THIS IS THE NAME OF YOUR HAMBURGER BUTTON

var slideNavName="slideDown"; //THIS IS THE NAME OF YOUR HIDING NAVIGATION

var rectangleName="rect"; //THIS IS THE NAME OF YOUR LITTLE HAMBURGER RECTANGLES, MINUS THE NUMBERS (SEE CSS COMMENTS)

//YOU MAY CHANGE ALL OF THESE IF YOU NEED TO (SEE CSS COMMENTS)
var showRect= "showRect";
var topRectX= "topRectX";
var hideRectX= "hideRectX";
var bottomRectX= "bottomRectX";


$("#"+hamburger).click(function(event) //CHECK IF YOUR BUTTON IS PRESSED
{
  if($("#"+slideNavName).attr('class')=="hidden") //CHECKS TO SEE IF YOUR MENU IS CURRENTLY CLOSED
  {
    //CHANGE ICON TO AN 'X'
    $("#"+rectangleName+"1").toggleClass(showRect+" "+topRectX);
    $("#"+rectangleName+"2").toggleClass(showRect+" "+hideRectX);
    $("#"+rectangleName+"3").toggleClass(showRect+" "+bottomRectX);
    
    //REVEAL YOUR NAVIGATION
    $("#"+slideNavName).toggleClass('hidden revealed');
  }
  
  else if($("#"+slideNavName).attr('class')=="revealed") //CHECKS TO SEE IF YOUR MENU IS CURRENTLY OPEN
  {
    //CHANGE ICON BACK INTO A HAMBURGER
    $("#"+rectangleName+"1").toggleClass(showRect+" "+topRectX);
    $("#"+rectangleName+"2").toggleClass(showRect+" "+hideRectX);
    $("#"+rectangleName+"3").toggleClass(showRect+" "+bottomRectX);
    
    //HIDE YOUR NAVIGATION
    $("#"+slideNavName).toggleClass('revealed hidden');
  }
});
//
//
//BONUS!!! AN OPENED NAV DISSAPEARS WHEN SCROLLING!!
//
//
//$(window).scroll(function(event) //AUTOMATICALLY HIDES THE NAV WHEN SCROLLING STARTS
//{
//  if($("#"+slideNavName).attr('class')=="revealed") //SEE IF YOUR NAV IS OPEN
//  {
//    //CHANGE ICON BACK INTO A HAMBURGER
//    $("#"+rectangleName+"1").toggleClass(showRect+" "+topRectX);
//    $("#"+rectangleName+"2").toggleClass(showRect+" "+hideRectX);
//    $("#"+rectangleName+"3").toggleClass(showRect+" "+bottomRectX);
//    
//    //HIDE YOUR NAVIGATION
//    $("#"+slideNavName).toggleClass('revealed hidden');
//  }
//});
//

jQuery(function( $ ){

	var addSearchTerm = function(myClass){
		var newST = '<div class="search-term ' + myClass + '"><div class="input-group"><a class="input-group-addon bg-danger removeST"><span class="glyphicon glyphicon-remove"></span>Remove</a><input name="' + myClass + '_publisher[]" type="text" class="form-control"><div class="input-group-btn"><button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"><name>Publisher</name><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right" role="menu"><li><a class="select-query" href="#">Title</a></li><li><a class="select-query" href="#">Creator</a></li><li><a class="select-query" href="#">Subject</a></li><li><a class="select-query" href="#">Description</a></li><li><a class="select-query" href="#">Publisher</a></li><li><a class="select-query" href="#">Contributor</a></li><li><a class="select-query" href="#">Type</a></li><li><a class="select-query" href="#">Format</a></li><li><a class="select-query" href="#">Identifier</a></li><li><a class="select-query" href="#">Source</a></li><li><a class="select-query" href="#">Language</a></li><li><a class="select-query" href="#">Relation</a></li><li><a class="select-query" href="#">Coverage</a></li><li><a class="select-query" href="#">Rights</a></li></ul></div><!-- /btn-group --></div><!-- /input-group --></div>';
		$("#termBoxes").append(newST);
	}
	$.fn.wrapInTag = function(opts) {

	  var tag = opts.tag || 'strong'
	    , words = opts.words || []
	    , regex = RegExp(words.join('|'), 'gi') // case insensitive
	    , replacement = '<'+ tag +'>$&</'+ tag +'>';

	  return this.html(function() {
	    return $(this).html().replace(regex, replacement);
	  });
	};

	$(".box-shadow-menu").click(function(){
	 	$(".menu").toggleClass("active");
	});

	$("#search-terms").on('click','.removeST', function(){
	 	$(this).parents(".search-term").remove();
	});

	$("#search-terms").on('click','.selectST', function(){
	 	var classToAdd = $(this).data("cta");
	 	addSearchTerm(classToAdd);
	});

	$("#termBoxes").on('click','.select-query', function(){
	 	var classToAdd = $(this).html();
	 	$(this).closest(".input-group").find("name").html(classToAdd);
	 	var prefix = $(this).closest(".input-group").find("input").attr('name').split('_')[0];
    if (prefix == 'and' || prefix == 'or' || prefix == 'not') {
  	 	$(this).closest(".input-group").find("input").attr('name', prefix + "_" + classToAdd.toLowerCase() +"[]");
    } else {
      $(this).closest(".input-group").find("input").attr('name', "or_" + classToAdd.toLowerCase() +"[]");
    }
  });

	$("input[name=options]").change(function(){
		console.log($("input[name=options]:checked").attr('id'));
		if ( $("input[name=options]:checked").attr('id') == 'range' ) {
			$("#fromDate").parent().parent().show();
			$("#toDate").parent().parent().show();
		} else if ( $("input[name=options]:checked").attr('id') == 'before' ) {
			$("#fromDate").val('');
			$("#fromDate").parent().parent().hide();
			$("#toDate").parent().parent().show();
		} else if ( $("input[name=options]:checked").attr('id') == 'after' ) {
			$("#fromDate").parent().parent().show();
			$("#toDate").val('');
			$("#toDate").parent().parent().hide();
		}
	});

 	$('.carou').slick({
  		infinite: true,
  		slidesToShow: 3,
  		slidesToScroll: 3,
  		responsive: [
  		    {
  		      breakpoint: 1024,
  		      settings: {
  		        slidesToShow: 3,
  		        slidesToScroll: 3,
  		        infinite: true,
  		        dots: true
  		      }
  		    },
  		    {
  		      breakpoint: 600,
  		      settings: {
  		        slidesToShow: 2,
  		        slidesToScroll: 2
  		      }
  		    },
  		    {
  		      breakpoint: 480,
  		      settings: {
  		        slidesToShow: 1,
  		        slidesToScroll: 1
  		      }
  		    }
  		  ]
	});


 	$('.scrolly').slick({
  		infinite: true,
  		slidesToShow: 4,
  		slidesToScroll: 4,
  		responsive: [
  		    {
  		      breakpoint: 1024,
  		      settings: {
  		        slidesToShow: 3,
  		        slidesToScroll: 3,
  		        infinite: true,
  		        dots: true
  		      }
  		    },
  		    {
  		      breakpoint: 600,
  		      settings: {
  		        slidesToShow: 2,
  		        slidesToScroll: 2
  		      }
  		    },
  		    {
  		      breakpoint: 480,
  		      settings: {
  		        slidesToShow: 1,
  		        slidesToScroll: 1
  		      }
  		    }
  		  ]
	});

	if (typeof textSearch !== 'undefined') {
		var results = textSearch.match(/("[^"]+"|[^"\s]+)/g);

	 	$('.bold-search-terms').wrapInTag({
		  tag: 'strong',
		  words: results
		});
	}

	if ($('#fromDate').length ) {
		$('#fromDate').datetimepicker({
		 timepicker:false,
		 format:'d-m-Y',
		 yearStart: 1900,
		 yearEnd: 2014
		});
		$('#toDate').datetimepicker({
		 timepicker:false,
		 format:'d-m-Y',
		 yearStart: 1900,
		 yearEnd: 2014
		});
	}
});

